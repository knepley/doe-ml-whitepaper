\documentclass[10pt]{article}

\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{color}
\usepackage{graphicx}
\usepackage{hyperref}

\textwidth 6.9in
\oddsidemargin -0.2in
\evensidemargin -0.2in

\setlength{\topmargin}{-0.5in}
\setlength{\textheight}{8.5in}

% IEEEtran has huge spaces between bibliography items. This trick lets us reduce them to save space.
\let\oldthebibliography=\thebibliography
\let\endoldthebibliography=\endthebibliography
\renewenvironment{thebibliography}[1]{%
  \begin{oldthebibliography}{#1}%
    %\setlength{\itemsep}{10pt plus 2.5pt minus 2.5pt} % old value
    \setlength{\itemsep}{0pt} % adjust as necessary
}{%
  %\bigskip {\bf itemsep \the\itemsep} % activate this line to find out what was set
  \end{oldthebibliography}%
}

\date{}

\begin{document}
%
% paper title
% can use linebreaks \\ within to get better formatting as desired
\title{ \vspace*{-1.2in}
The Kolmogorov Superposition Theorem for Machine Learning\\[.5em]
{\small A White Paper Submitted to SciML2018\\DOE ASCR Workshop on Scientific Machine Learning\\January 2018\\[-1.0em] }}
% author names and affiliations
% use a multiple column layout for up to two different
% affiliations

\author{
  {\small Matthew G. Knepley$^1$, Jonas Actor$^2$, Paul Bauman$^1$, and Mark Adams$^3$}\\
  {\small $^1$University at Buffalo, $^2$Rice University, $^3$Lawrence Berkeley National Laboratory} \\
  {\footnotesize \tt Presenting Author: \href{mailto:knepley@buffalo.edu}{knepley@buffalo.edu}}
}

\maketitle

\vspace{-.3in}

%Position papers should describe relevant existing, or opportunities for, applied mathematics research that has a clear potential to enhance the mathematical understanding of machine learning, particularly in terms of the rigor and robustness of scientific machine learning. The papers should clearly address the following points:
%Key Challenges: What challenges related to mathematical aspects of scientific machine learning is the paper intended to address?
%New Research Directions: What is the potential for new mathematical research to address the challenge?
%State of the Art: Set the new ideas in context by describing the state of the art relating to the proposed research direction.

%This description should be followed by an assessment of potential research directions based on the following dimensions:
%Maturity: Are there existing mathematical methods or research directions that address the challenge(s) and that show promise for scientific machine learning? What are the indicators that a given method or approach will address the identified challenges? If there are not existing methods or research approaches to meeting the challenge, can you suggest ways to gain new insight into the problem space?
%Uniqueness: Is the identified challenge unique to scientific applications of machine learning? What makes it so?
%Novelty: Is the challenge being addressed by other research programs? By the private sector? Why should this challenge be of interest to the ASCR Applied Mathematics program?

\paragraph{Challenge} For a classification problem, we want ultimately a function $f$ mapping our high dimensional input
to a scalar output. We will assume a bounded input and rescale so that we map from the unit $n$-cube $\mathbb{E}^n$ to
the real line, $f:\mathbb{E}^n \to \mathbb{R}$. Moreover, we will only ask for significant separation of the outputs for
different classes so that the function $f$ itself can be continuous, as the constructive approximation theory for
continuous functions is tractable. The clear challenge here is often called the Curse of Dimensionality, namely that
$h^{-n}$ data is necessary to approximate the function with resolution $h$.

\paragraph{Research Direction} A venerable approximation theory result from Kolmogorov~\cite{Kolmogorov1957} states that
our classification function from above may be represented as
\begin{align}
  f(x_1, \dots, x_n) = \sum^{2n}_{q=0} \chi_q \left( \sum^n_{p=1} \psi_{pq}(x_p) \right),
\end{align}
where $\chi_q$ and $\psi_{pq}$ are univariate continuous functions. There was considerable theoretical work on this
question in the two decades after the result. It was shown that the \textit{inner functions} $\psi_{pq}$ can be
represented by shifts of a single, universal function~\cite{Sprecher1972}, so that
\begin{align}
  f(x_1, \dots, x_n) = \sum^{2n}_{q=0} \chi_q \left( \sum^n_{p=1} \lambda^p \psi(x_p + q \epsilon) \right),
\end{align}
where $\lambda$ is a constant whose powers are algebraically independent, and $\epsilon \le 1/2n$. It was noted very
early on that the inner $\psi$ function cannot be differentiable~\cite{Vitushkin1954} and this led early researchers to
conclude that the representation was unsuitable for computation~\cite{DiaconisShahshahani1984}. This conclusion is still
the accepted wisdom today~\cite{BraunGriebel2009,Griebel2006}, and algorithms such as sparse grids~\cite{Griebel2006}, tensor-train
decomposition~\cite{Oseledets2011}, deep neural networks~\cite{LeCunBengioHinton2015} are being pursued instead. However, there appears to be a neglected path forward.

\paragraph{New Direction} In 1967~\cite{Fridman1967}, Fridman proved that the inner functions could be chosen to be Lipschitz with constant unity,
and that this was the optimal regularity attainable. Unfortunately, he did not provide an explicit construction. Thus
all subsequent work used the constructive version of
Sprecher~\cite{HechtNielsen1987,HechtNielsen1989,GirosiPoggio1989,Koppen2002,BraunGriebel2009}. Unfortunately, the inner
function that Sprecher constructs is H\"older with exponent $\ln 2/\ln(2n + 2)$. This irregularity explodes the storage
needed to represent the function~\cite{Griebel2006,Bryant2008} and also the time to synthesize the function $f$ from the
representation~\cite{LeniFougerolleTruchetet2009}. Moreover, if the inner function is H\"older, it can be shown that the
modulus of continuity of the outer functions $\chi_q$ degrades from that of the inner function~\cite{Liu2015}.

Recently~\cite{ActorKnepley2018}, we have explicitly constructed a Lipschitz inner function with Lipschitz constant
unity. The construction is based upon a non-uniform division of the interval, and is not possible for the uniform
division used in the proof of Kolmogorov and the construction of Sprecher. We note that Lipschitz functions are
differentiable almost everywhere, so that while $\psi$ is not $C^1$ (as prescribed by the theorem of Vitushkin), it is
efficiently approximable~\cite{Trefethen2013}. Moreover, Lipschitz functions are closed under composition, so that the
outer function will be Lipschitz when representing a Lipschitz input $f$. This means that the outer functions $\chi_q$
may be efficiently approximated using tools such as Chebfun~\cite{DriscollHaleTrefethen2014} or the finite element
method~\cite{BrennerScott02}.

In addition to offering an efficient route to classification and high dimensional regression, we believe that the
Kolmogorov Superposition Theorem (KST) can shed light on the amazing success of neural networks (NN). Hecht-Nielsen
noted that the structure of the KST representation is that of a three layer, feed-forward
NN~\cite{HechtNielsen1987,HechtNielsen1989}. Considerable approximation theory work has shown that smooth activation
functions render many continuous functions inapproximable~\cite{Vitushkin1954,DiaconisShahshahani1984}. The necessity of
non-smooth inner functions in the KST could explain the prevalence of non-smooth activation functions. In fact, the
design of optimal activation functions would be aided by looking at $\psi$, which can be thought of as a universal
change of basis. In addition, the KST can be seen as an optimial compression of a deep NN intended to present continuous
functions. This kind of compression could aid in explaining the content of a NN after it has learned a function. It may
be possible to re-express a given NN in the Kolmogorov basis in order to both understand what learning has constructed,
and also improve the robustness of the learning process.

\bibliographystyle{siam}
\bibliography{petsc,petscapp}
\end{document}
